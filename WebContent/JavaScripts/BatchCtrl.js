/*******************************************************************************
 * 
 * TO-DO
 * 	- FORECASTING  
 * 					- pass the status from the table to the modal so that we don't
 * 					have to use radio buttons to map an associate or move them
 * 					back to available
 *  			   - asynchronously refresh the totals table after changing 
 *  				associate status
 *  - CURRENT		
 *  				- asynchronously refresh the totals table after changing
 *  				associate status - code is towards the bottom of this page 
 *  - VALIDATION
 *  				- add input validation for adding associates/batches/clients
 * 
 ******************************************************************************/



/*******************************************************************************
 * 
 * All the controllers
 * 
 ******************************************************************************/
var mainApp = angular.module('superuser', []);

mainApp.controller("infoTable", function($scope, $http) {

	var subm;	 
	 
		/*
		 * All the following is for the current tab.
		 * Return the number of Available associate
		 */
			$http.get("/StagingManagementSystem/displayCurrentJavaAvailable").then(function(result) {
				$scope.javaAvailable = result.data;
			});
			$http.get("/StagingManagementSystem/displayCurrentNETAvailable").then(function(result) {
				$scope.netAvailable = result.data;
			});	
			$http.get("/StagingManagementSystem/displayCurrentSDETAvailable").then(function(result) {
				$scope.sdetAvailable = result.data;
			});
			
			/*
			 * Return the number of Mapped associate
			 */
			$http.get("/StagingManagementSystem/displayCurrentJavaMapped").then(function(result) {
				$scope.javaMapped = result.data;
			});
			$http.get("/StagingManagementSystem/displayCurrentNETMapped").then(function(result) {
				$scope.netMapped = result.data;
			});	
			$http.get("/StagingManagementSystem/displayCurrentSDETMapped").then(function(result) {
				$scope.sdetMapped = result.data;
			});
			
			/*
			 * Return the number of Confirmed associate
			 */
			$http.get("/StagingManagementSystem/displayCurrentJavaConfirmed").then(function(result) {
				$scope.javaConfirmed = result.data;
			});
			$http.get("/StagingManagementSystem/displayCurrentNETConfirmed").then(function(result) {
				$scope.netConfirmed = result.data;
			});	
			$http.get("/StagingManagementSystem/displayCurrentSDETConfirmed").then(function(result) {
				$scope.sdetConfirmed = result.data;
			});
			
			/*
			 * Return the List of Available associate
			 */
			
			$http.get("/StagingManagementSystem/displayCurrentJavaAvailableList").then(function(result) {
				$scope.javaAvailableList = result.data;
			});
			$http.get("/StagingManagementSystem/displayCurrentNETAvailableList").then(function(result) {
				$scope.netAvailableList = result.data;
			});	
			$http.get("/StagingManagementSystem/displayCurrentSDETAvailableList").then(function(result) {
				$scope.sdetAvailableList = result.data;
			});
			/*
			 * Return the List of Mapped associate
			 */
			$http.get("/StagingManagementSystem/displayCurrentJavaMappedList").then(function(result) {
				$scope.javaMappedList = result.data;
			});
			$http.get("/StagingManagementSystem/displayCurrentNETMappedList").then(function(result) {
				$scope.netMappedList = result.data;
			});	
			$http.get("/StagingManagementSystem/displayCurrentSDETMappedList").then(function(result) {
				$scope.sdetMappedList = result.data;
			});
			
			/*
			 * Return the List of Confirmed associate
			 */
			$http.get("/StagingManagementSystem/displayCurrentJavaConfirmedList").then(function(result) {
				$scope.javaConfirmedList = result.data;
			});
			$http.get("/StagingManagementSystem/displayCurrentNETConfirmedList").then(function(result) {
				$scope.netConfirmedList = result.data;
			});	
			$http.get("/StagingManagementSystem/displayCurrentSDETConfirmedList").then(function(result) {
				$scope.sdetConfirmedList = result.data;
			});
	//	}

			/*
			 * display a list of clients in the forecasting/current modals
			 */
		$http.get("/StagingManagementSystem/displayClients").then(function(result) {
		$scope.clientList = result.data;
		$scope.lastItem = {
			name : "Select Client"
		};
		// add a last object the list of client to inform the user the select a
		// value.
		$scope.clientList.push($scope.lastItem);
		console.log($scope.clientList);
	});

	$scope.associateSelected = [];

	$scope.exist = function(item) {
		return $scope.associateSelected.indexOf(item) > -1;
	}
	// de-select and select associate accordingly.
	$scope.toggleSelection = function(item) {
		var idx = $scope.associateSelected.indexOf(item);

		if (idx > -1) {
			$scope.associateSelected.splice(idx, 1);
		} else {
			$scope.associateSelected.push(item);
		}
	}

	// this may or may be used
	$http.get("/StagingManagementSystem/displayStats").then(function(result) {
		$scope.associatesList = result.data;
		console.log($scope.associatesList);

	});

	// hide the number buttons on forecasting until a month has been selected
	$(".associateBtn").hide();

	/*
	 * FORECASTING: when a month is clicked:
	 * 	 display the totals inside of the buttons
	 */
	
	$(".month").on("click",	function(e) {
		
		// set subm to the id of the month button that was clicked
		subm = e.target.id;

		// call the getMonth method
		$http.get("/StagingManagementSystem/getMonth?month=" + subm)
		.then(function(result) {
			$scope.month = result.data;
			console.log(month);
			console.log(result.data);
		});

		/*
		 * FORECASTING: when a number button is clicked
		 * open the forecasting modal
		 */
		$(".associateBtn").on("click", function(e) {
			var status = e.target.id;
			var type = e.target.name;

			// get the list of associates for that status and that type
			$http.get("/StagingManagementSystem/getAssociates?month="
			+ subm + "&status=" + status + "&type=" + type).then(
			function(result) {
				$scope.associates = result.data;
				console.log(associates);
				console.log(result.data);
			});

			// set the header of the modal
			$(".statusheader").html(status);
			$(".typeheader").html(type);
			var stat = $("#status").val(status);

		});

		// show the associate buttons once a month has been clicked
		$(".associateBtn").show();

		/*
		 * FORECASTING: submit associates to have their status changed
		 */				
		$scope.submitAssociates = function() {
					// validation the make sure the select a radio button.
					var status = $scope.modifyStatus.status;
					var clientName;

					if ($scope.associateSelected.length != "0") {
						// validate to check if a client and an associate was
						// selected.
						var associateIds = [];
						// add the status and the client to the list of item to
						// send to
						// the rest-Controller
						if (status == "Available") {
							clientName = $('#sel1').val();
						}
						// this add only the Ids of the associates selected.
						angular.forEach($scope.associateSelected, function(
								value, index) {
							associateIds.push(value.associateID);
						});
						// this add a label to the array created.
						var data = {
							associateId : associateIds,
							status : status
						};

						// convert into a JSON object ( which is a String ).
						var submittedData = JSON.stringify(data);
						console.log(submittedData);
						// make a call to the controller to send the array full
						// of
						// data
						$http.post("/StagingManagementSystem/updateAssociates",
								submittedData).success(function(data) {
							// on success, reset the modal values.
							$scope.associateSelected = [];
							$scope.modifyStatus.status = "";
							alert("Associate(s) Successfully Updated");

							// close the modal
							$('#ForecastModal').modal('hide');
							
							// find some way to refresh the table asynchronously
							
						}).error(function(data) {
							alert("Associate(s) did not Update Successfully");
						});
					} else
						alert("Please select an associate!");

				};
			});
	
	/*
	 * CURRENT: submit available associates to have their status changed to mapped
	 */
	
	$scope.onSubmitAvailable = function() {
		var clientName = $scope.modifyStatus.clientName ;
			// validate to check if a client and an associate was selected.
		if(	$scope.associateSelected.length != "0") {
			if (clientName != "Select Client"){
				var associateIds = [];
				var client;
				// add the status and the client to the list of item to send to
				// the rest-Controller
				client = $scope.modifyStatus.clientName;
				// this add only the Ids of the associates selected.
				angular.forEach($scope.associateSelected,
						function(value, index) {
							associateIds.push(value.associateID);
						});
				// this add a label to the array created.
				var data = {
					associateId : associateIds,
					client: client
				};

				// convert into a JSON object ( which is a String ).
				var submittedData = JSON.stringify(data);
				// make a call to the Rest-controller to send the array full of
				// data
				$http.post("/StagingManagementSystem/updateAvailableAssociates", submittedData )
				.success(function(data) {
					// on success, reset the modal values.
					$scope.associateSelected = [];
					$scope.modifyStatus.clientName = "Select Client";
					alert("Associate(s) Successfully Updated");
					$("#AvailableJavaAssociateInfo").modal('hide');
					refresh();
				}).error(function(data) {
					alert("Associate(s) did not Update Successfully");
				});
				
			}else alert("Please select a Client!");
	} else
				alert("Please select an associate");
	};
	
	/*
	 * CURRENT: submit the mapped associates to have their status changed to available or confirmed
	 */
	
	$scope.onSubmitMapped = function() {
		// validation the make sure the select a radio button.
		var c = document.getElementById('test2').checked;
		var a = document.getElementById('test3').checked;
		var clientName = document.getElementById('sel1').value;

		if($scope.associateSelected.length != "0") {
		if ( c || a) {
			// validate to check if a client and an associate was selected.
				var associateIds = [];
				var status;
				// add the status and the client to the list of item to send to
				// the rest-Controller
				status = $scope.modifyStatus.status;
				// this add only the Ids of the associates selected.
				angular.forEach($scope.associateSelected,
						function(value, index) {
							associateIds.push(value.associateID);
						});
				// this add a label to the array created.
				var data = {
					associateId : associateIds,
					status: status
				};
				
				// convert into a JSON object ( which is a String ).
				var submittedData = JSON.stringify(data);
				console.log(submittedData);
				// make a call to the controller to send the array full of
				// data
				$http.post("/StagingManagementSystem/updateMappedAssociates", submittedData )
				.success(function(data) {
					// on success, reset the modal values.
					$scope.associateSelected = [];
					$scope.modifyStatus.status = "";
					alert("Associate(s) Successfully Updated");
					refresh();
				}).error(function(data) {
					alert("Associate(s) did not Update Successfully");
				});
			} else
				alert("Please select a status!");
		} else
			alert("Please select an associate!");

	};
	
	/*
	 * CURRENT: submit associates to have their status changed from confirmed to available
	 */
	
	$scope.onSubmitConfirmed = function() {
		if(	$scope.associateSelected.length != "0") {
				var associateIds = [];

				// this add only the Ids of the associates selected.
				angular.forEach($scope.associateSelected,
						function(value, index) {
							associateIds.push(value.associateID);
						});
				// this add a label to the array created.
				var data = {
					associateId : associateIds
				};

				// convert into a JSON object ( which is a String ).
				var submittedData = JSON.stringify(data);
				// make a call to the Rest-controller to send the array full of
				// data
				$http.post("/StagingManagementSystem/updateConfirmedAssociates", submittedData )
				.success(function(data) {
					// on success, reset the modal values.
					$scope.associateSelected = [];
				//	$scope.modifyStatus.clientName = "Select Client";
					alert("Associate(s) Successfully Updated");
					refresh();
				}).error(function(data) {
					alert("Associate(s) did not Update Successfully");
				});
			} else
				alert("Please select an associate!");

	};
	
	/*
	 * CURRENT: refresh the table after associate status has been changed
	 * 			will update the modal numbers but does not actually change
	 * 			the table stats
	 */
	function refresh(){
		$http.get("/StagingManagementSystem/displayCurrentJavaAvailable").then(function(result) {
			$scope.javaAvailable = result.data;
		});
		$http.get("/StagingManagementSystem/displayCurrentNETAvailable").then(function(result) {
			$scope.netAvailable = result.data;
		});	
		$http.get("/StagingManagementSystem/displayCurrentSDETAvailable").then(function(result) {
			$scope.sdetAvailable = result.data;
		});
		
		/*
		 * Return the number of Mapped associate
		 */
		$http.get("/StagingManagementSystem/displayCurrentJavaMapped").then(function(result) {
			$scope.javaMapped = result.data;
		});
		$http.get("/StagingManagementSystem/displayCurrentNETMapped").then(function(result) {
			$scope.netMapped = result.data;
		});	
		$http.get("/StagingManagementSystem/displayCurrentSDETMapped").then(function(result) {
			$scope.sdetMapped = result.data;
		});
		
		/*
		 * Return the number of Confirmed associate
		 */
		$http.get("/StagingManagementSystem/displayCurrentJavaConfirmed").then(function(result) {
			$scope.javaConfirmed = result.data;
		});
		$http.get("/StagingManagementSystem/displayCurrentNETConfirmed").then(function(result) {
			$scope.netConfirmed = result.data;
		});	
		$http.get("/StagingManagementSystem/displayCurrentSDETConfirmed").then(function(result) {
			$scope.sdetConfirmed = result.data;
		});
		
		/*
		 * Return the List of Available associate
		 */
		
		$http.get("/StagingManagementSystem/displayCurrentJavaAvailableList").then(function(result) {
			$scope.javaAvailableList = result.data;
		});
		$http.get("/StagingManagementSystem/displayCurrentNETAvailableList").then(function(result) {
			$scope.netAvailableList = result.data;
		});	
		$http.get("/StagingManagementSystem/displayCurrentSDETAvailableList").then(function(result) {
			$scope.sdetAvailableList = result.data;
		});
		/*
		 * Return the List of Mapped associate
		 */
		$http.get("/StagingManagementSystem/displayCurrentJavaMappedList").then(function(result) {
			$scope.javaMappedList = result.data;
		});
		$http.get("/StagingManagementSystem/displayCurrentNETMappedList").then(function(result) {
			$scope.netMappedList = result.data;
		});	
		$http.get("/StagingManagementSystem/displayCurrentSDETMappedList").then(function(result) {
			$scope.sdetMappedList = result.data;
		});
		
		/*
		 * Return the List of Confirmed associate
		 */
		$http.get("/StagingManagementSystem/displayCurrentJavaConfirmedList").then(function(result) {
			$scope.javaConfirmedList = result.data;
		});
		$http.get("/StagingManagementSystem/displayCurrentNETConfirmedList").then(function(result) {
			$scope.netConfirmedList = result.data;
		});	
		$http.get("/StagingManagementSystem/displayCurrentSDETConfirmedList").then(function(result) {
			$scope.sdetConfirmedList = result.data;
		});

};

});

/*
 * ADD AN ASSOCIATE: displays a list of batches to add an associate to
 */
mainApp.controller("BatchCtrl", function($scope, $http) {

	$http.get("/StagingManagementSystem/displayBatch").then(function(result) {
		$scope.batches = result.data;
		console.log(batches);
		console.log(result.data);
	});
});
