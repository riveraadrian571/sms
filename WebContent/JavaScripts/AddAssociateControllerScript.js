/* 
 * FORECASTING: show and hide radio buttons in the forecasting modal 
 * based on whether associates are listed as available/mapped/confirmed
*/

// available: show mapped radio, client input select
// hide available radio, confirmed radio, and available column
$(document).on("click", "#Available", function() {
	$("#availableRadio").hide();
	$("#mappedRadio").show();
	$("#sel1").show();
	$("#confirmedRadio").hide();
	$('#availablecolumn').hide();
	$("#statusField").val("Mapped");
});

//available: show available radio, confirmed radio, available column
//hide mapped radio, client input select, and available column
$(document).on("click", "#Mapped", function() {
	$("#availableRadio").show();
	$("#mappedRadio").hide();
	$("#sel1").hide();
	$("#confirmedRadio").show();
	$('#availablecolumn').show();

});

//available: show available radio, available radio, available column
//hide mapped radio, client input select, and confirmed column
$(document).on("click", "#Confirmed", function() {
	$("#availableRadio").show();
	$("#mappedRadio").hide();
	$("#sel1").hide();
	$("#confirmedRadio").hide();
	$('#availablecolumn').show();
	$("#statusField").val("Available");
});

/* 
 * add a batch to the database 
 */
$(document).on("click", "#addBatch", function() {

	$("#addBatchForm").unbind('submit').bind('submit', function(e) {
		e.preventDefault();

		$.ajax({
			type : 'POST',
			url : '/StagingManagementSystem/addBatch',
			data : $(this).serialize(),
			success : function(data) {
				console.log("SUCCESS: ", data);
			    $('#addBatch').modal('hide');

			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	});
});

/* 
 * add an associate to the database 
 */
		$(document).on("click", "#addAssociate", function() {

			$("#addAssociateForm").unbind('submit').bind('submit', function(e) {
				e.preventDefault();

				$.ajax({
					type : 'POST',
					url : '/StagingManagementSystem/addAssociate',
					data : $(this).serialize(),
					success : function(data) {
						console.log("SUCCESS: ", data);
						$('#addAssociate').modal('hide');

					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			});
		});

		/* 
		 * add a client to the database 
		 */
		$(document).on("click", "#addClient", function() {

			$("#addClientForm").unbind('submit').bind('submit', function(e) {
				e.preventDefault();

				$.ajax({
					type : 'POST',
					url : '/StagingManagementSystem/addClient',
					data : $(this).serialize(),
					success : function(data) {
						console.log("SUCCESS: ", data);
						$('#addClient').modal('hide');

					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			});
		});
		
		
		/* 
		 * datatable things 
		 */
		$(document).ready(function() {
		    $('#myTable').DataTable();
		} );
