<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html ng-app="superuser">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Manager View</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/modalView.css" />

<link rel="stylesheet" href="css/login.css" />

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>SMS</title>
<!-- AngularJs Library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.min.js"></script>

<!-- DataTable Library -->
<link type="text/css" rel="stylesheet"
	href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">

<!-- Angular Table Library -->
<link rel="stylesheet"
	href="https://unpkg.com/ng-table@2.0.2/bundles/ng-table.min.css">

<script type="text/javascript"
	src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>



<!-- Import Javascript Files -->
<script src="JavaScripts/BatchCtrl.js"></script>
<script src="JavaScripts/AddAssociateControllerScript.js"></script>
<title>Manager</title>

</head>
<body>
	<jsp:include page="ForecastModals.jsp" />
	<jsp:include page="CurrentModals.jsp" />

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand">
				<img src="http://i.imgur.com/WTYxaWk.png" alt="Revature" width="10%" /></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right text-uppercase">
					<!-- Manager dropdown-->
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>
							Manager </a>
					<!-- navbar link logout -->
							<li><a href="javascript:formSubmit()"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>

							<script>
								function formSubmit() {
									document.getElementById("logoutForm")
											.submit();
								}
							</script>

							<!-- csrt for log out-->
							<form action="logout" method="post" id="logoutForm">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</form>

				</ul>
			</div>
		</div>
	</nav>
	

<div class="row" ng-controller="infoTable">

		<!-- right side bar -->
		<div class="col-sm-offset-2 col-sm-8">
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#current">Current</a></li>
				<li><a data-toggle="tab" href="#forecast"
					ng-click="getForecast()">Forecast</a></li>
			</ul>

			<div class="tab-content">
				<div id="current" class="tab-pane fade in active" >
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th>Status</th>
								<th>Java</th>
								<th>.NET</th>
								<th>JTA</th>
								
								<!-- Not currently available. 
								<th>Big Data</th> -->
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Available</td>
								<td>
									<button id="available" data-toggle="modal" class="btn btn-xs" data-target="#AvailableJavaAssociateInfo">{{javaAvailable}}</button>
								</td>
								<td><button id="available" data-toggle="modal" class="btn btn-xs"
										data-target="#AvailableNetAssociateInfo">{{netAvailable}}</button></td>
								<td><button id="available" data-toggle="modal" class="btn btn-xs"
										data-target="#AvailableSDETAssociateInfo">{{sdetAvailable}}</button></td>
								
								<!-- Not currently available.
								<td><button id="available" data-toggle="modal"
										data-target="#AssociateInfo">{{bigDataAvailable}}</button></td> -->
							</tr>
							<tr>
								<td>Mapped</td>
								<td><button id="mapped" data-toggle="modal" class="btn btn-xs"
										data-target="#MappedJavaAssociateInfo">{{javaMapped}}</button></td>
								<td><button id="mapped" data-toggle="modal" class="btn btn-xs"
										data-target="#MappedNetAssociateInfo">{{netMapped}}</button></td>
								<td><button id="mapped" data-toggle="modal" class="btn btn-xs"
										data-target="#MappedSDETAssociateInfo">{{sdetMapped}}</button></td>
										
								<!-- Not currently available. 
								<td><button id="mapped" data-toggle="modal"
										data-target="#AssociateInfo">{{bigDataMapped}}</button></td> -->
							</tr>
							<tr>
								<td>Confirmed</td>
								<td><button id="confirmed" data-toggle="modal" class="btn btn-xs"
										data-target="#ConfirmedJavaAssociateInfo">{{javaConfirmed}}</button></td>
								<td><button id="confirmed" data-toggle="modal" class="btn btn-xs"
										data-target="#ConfirmedNetAssociateInfo">{{netConfirmed}}</button></td>
								<td><button id="confirmed" data-toggle="modal" class="btn btn-xs"
										data-target="#ConfirmedSDETAssociateInfo">{{sdetConfirmed}}</button></td>
										
								<!-- Not currently available.
								<td><button id="confirmed" data-toggle="modal"
										data-target="#AssociateInfo">{{bigDataConfirmed}}</button></td> -->
							</tr>

						</tbody>
					</table>
				</div>


				<div id="forecast" class="tab-pane fade">
					<table class="table table-bordered table-striped table-hover">
						<thead>
						<thead>
							<tr>
								<th>Status</th>
								<th>Java</th>
								<th>.NET</th>
								<th>JTA</th>
							</tr>
						</thead>
						</thead>
						<tbody id="showForecast">
						
							<tr>
								<td>Available</td>
								<td>
									<button data-toggle="modal" class="associateBtn btn btn-xs"
										id="Available" name="JAVA" data-target="#ForecastModal">{{month[0]}}</button>
								</td>
								<td><button data-toggle="modal"
										class="associateBtn btn btn-xs" id="Available" name=".NET"
										data-target="#ForecastModal">{{month[1]}}</button></td>
								<td><button data-toggle="modal"
										class="associateBtn btn btn-xs" id="Available" name="SDET"
										data-target="#ForecastModal">{{month[2]}}</button></td>

							</tr>
							<tr>
								<td>Mapped</td>
								<td>
									<button data-toggle="modal" class="associateBtn btn btn-xs"
										id="Mapped" name="JAVA" data-target="#ForecastModal">{{month[3]}}</button>
								</td>
								<td><button data-toggle="modal"
										class="associateBtn btn btn-xs" id="Mapped" name=".NET"
										data-target="#ForecastModal">{{month[4]}}</button></td>
								<td><button data-toggle="modal"
										class="associateBtn btn btn-xs" id="Mapped" name="SDET"
										data-target="#ForecastModal">{{month[5]}}</button></td>

							</tr>
							<tr>
								<td>Confirmed</td>
								<td>
									<button data-toggle="modal" class="associateBtn btn btn-xs"
										id="Confirmed" name="JAVA" data-target="#ForecastModal">{{month[6]}}</button>
								</td>
								<td><button data-toggle="modal"
										class="associateBtn btn btn-xs" id="Confirmed" name=".NET"
										data-target="#ForecastModal">{{month[7]}}</button></td>
								<td><button data-toggle="modal"
										class="associateBtn btn btn-xs" id="Confirmed" name="SDET"
										data-target="#ForecastModal">{{month[8]}}</button></td>

							</tr>
						</tbody>
					</table>
					<ul class="nav nav-pills" style="background-color: #474c55;">
						<center><div class="btn-group">
							<button id="1" class="month btn btn-primary">January</button>
							<button id="2" class="month btn btn-primary">February</button>
							<button id="3" class="month btn btn-primary">March</button>
							<button id="4" class="month btn btn-primary">April</button>
							<button id="5" class="month btn btn-primary">May</button>
							<button id="6" class="month btn btn-primary">June</button>
							<button id="7" class="month btn btn-primary">July</button>
							<button id="8" class="month btn btn-primary">August</button>
							<button id="9" class="month btn btn-primary">September</button>
							<button id="10" class="month btn btn-primary">October</button>
							<button id="11" class="month btn btn-primary">November</button>
							<button id="12" class="month btn btn-primary">December</button>
						</div></center>
					</ul>
				</div>
			</div>
		</div>
	</div>

</body>
</html>