<!-----------------------
	 Available Java associates modal
 ------------------------->
<div ng-controller="infoTable">
	<div id="AvailableJavaAssociateInfo" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">

				<form ng-submit="onSubmitAvailable()">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" style="text-align: center">Available
							Associates</h4>
					</div>
					<div class="modal-body">

						<div>
							<div class="form-group">
								<label>Search</label> <input type="text" ng-model="search"
									placeholder="search">
							</div>
							<table ng-table="vm.tableParams" show-filter="true"
								class="table table-striped">
								<thead>
									<tr>
										<th>Check</th>
										<th>Name</th>
										<th>Employee ID</th>
										<th>Status</th>
										<th>Start Date</th>
										<th>End Date</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="a in javaAvailableList |filter:search">
										<td><input ng-model="associateSelected" type="checkbox"
											ng-checked="exist(a)" ng-click="toggleSelection(a)"
											ng-true-value="{{a.associateID}}"></td>
										<td>{{a.associateName}}</td>
										<td>{{a.associateID}}</td>
										<td>{{a.status}}</td>
										<td>{{a.batch.startDate}}</td>
										<td>{{a.batch.endDate}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<select ng-model="modifyStatus.clientName" class="form-control"
								id="sel1" name="clients" required>
								<option ng-repeat="t in clientList" value="{{t.clientID}}"
									selected>{{t.name}} {{t.location}}</option>
							</select>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-default">Map
								Associate</button>

							<button type="button" class="btn btn-default"
								data-dismiss="modal" ng-click="infoTable.refresh()">Close</button>
						</div>
					</div>
			</div>
			</form>
		</div>

	</div>
</div>

<!-----------------------
	 Mapped Java associates modal
 ------------------------->
<div id="MappedJavaAssociateInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">

			<form ng-submit="onSubmitMapped()">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center">Mapped
						Associates</h4>
				</div>
				<div class="modal-body">

					<div>
						<div class="form-group">
							<label>Search</label> <input type="text" ng-model="search"
								placeholder="search">
						</div>
						<table ng-table="vm.tableParams" show-filter="true"
							class="table table-striped">
							<thead>
								<tr>
									<th>Check</th>
									<th>Name</th>
									<th>Employee ID</th>
									<th>Status</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="a in javaMappedList |filter:search">
									<td><input ng-model="associateSelected" type="checkbox"
										ng-checked="exist(a)" ng-click="toggleSelection(a)"
										ng-true-value="{{a.associateID}}"></td>
									<td>{{a.associateName}}</td>
									<td>{{a.associateID}}</td>
									<td>{{a.status}}</td>
									<td>{{a.batch.startDate}}</td>
									<td>{{a.batch.endDate}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<!--  The table below is the to how the dropdown button as well as the radio buttons are positioned -->
					<div class="col-md-6">

						<label id="availableRadio" class="radio-inline"> <input
							ng-model="modifyStatus.status" type="radio" value="Available"
							name="optradio" id="test3"> Available

						</label> <label id="confirmedRadio" class="radio-inline"> <input
							ng-model="modifyStatus.status" type="radio" value="Confirmed"
							name="optradio" id="test2"> Confirmed
						</label>
					</div>
					<div class="col-md-6">

						<button type="submit" class="btn btn-default">Change Status</button>

						<button type="button" class="btn btn-default" data-dismiss="modal"
							ng-click="infoTable.refresh()">Close</button>
					</div>S
				</div>
			</form>
		</div>

	</div>
</div>
<!-----------------------
	 Confirmed Java associates modal
 ------------------------->
<div id="ConfirmedJavaAssociateInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">

			<form ng-submit="onSubmitConfirmed()">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center">Confirmed
						Associates</h4>
				</div>
				<div class="modal-body">

					<div>
						<div class="form-group">
							<label>Search</label> <input type="text" ng-model="search"
								placeholder="search">
						</div>
						<table ng-table="vm.tableParams" show-filter="true"
							class="table table-striped">
							<thead>
								<tr>
									<th>Check</th>
									<th>Name</th>
									<th>Employee ID</th>
									<th>Status</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="a in javaConfirmedList |filter:search">
									<td><input ng-model="associateSelected" type="checkbox"
										ng-checked="exist(a)" ng-click="toggleSelection(a)"
										ng-true-value="{{a.associateID}}"></td>
									<td>{{a.associateName}}</td>
									<td>{{a.associateID}}</td>
									<td>{{a.status}}</td>
									<td>{{a.batch.startDate}}</td>
									<td>{{a.batch.endDate}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default">Move to
							Available</button>

						<button type="button" class="btn btn-default" data-dismiss="modal"
							ng-click="infoTable.refresh()">Close</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
<!-----------------------
	 Available .Net associates modal
 ------------------------->
<div id="AvailableNetAssociateInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">

			<form ng-submit="onSubmitAvailable()">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center">Available
						Associates</h4>
				</div>
				<div class="modal-body">

					<div>
						<div class="form-group">
							<label>Search</label> <input type="text" ng-model="search"
								placeholder="search">
						</div>
						<table ng-table="vm.tableParams" show-filter="true"
							class="table table-striped">
							<thead>
								<tr>
									<th>Check</th>
									<th>Name</th>
									<th>Employee ID</th>
									<th>Status</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="a in netAvailableList |filter:search | limitTo:5">
									<td><input ng-model="associateSelected" type="checkbox"
										ng-checked="exist(a)" ng-click="toggleSelection(a)"
										ng-true-value="{{a.associateID}}"></td>
									<td>{{a.associateName}}</td>
									<td>{{a.associateID}}</td>
									<td>{{a.status}}</td>
									<td>{{a.batch.startDate}}</td>
									<td>{{a.batch.endDate}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-12" style="margin-bottom: 10px;">

						<select ng-model="modifyStatus.clientName" class="form-control"
							id="sel1" name="clients" required>
							<option ng-repeat="t in clientList" value="{{t.clientid}}"
								selected>{{t.name}} {{t.location}}</option>
						</select>

						<button type="submit" class="btn btn-default">Map Associates</button>

						<button type="button" class="btn btn-default" data-dismiss="modal"
							ng-click="infoTable.refresh()">Close</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>

<!-----------------------
	 Mapped .Net associates modal
 ------------------------->
<div id="MappedNetAssociateInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">

			<form ng-submit="onSubmitMapped()">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center">Mapped
						Associates</h4>
				</div>
				<div class="modal-body">

					<div>
						<div class="form-group">
							<label>Search</label> <input type="text" ng-model="search"
								placeholder="search">
						</div>
						<table ng-table="vm.tableParams" show-filter="true"
							class="table table-striped">
							<thead>
								<tr>
									<th>Check</th>
									<th>Name</th>
									<th>Employee ID</th>
									<th>Status</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="a in netMappedList |filter:search | limitTo:5">
									<td><input ng-model="associateSelected" type="checkbox"
										ng-checked="exist(a)" ng-click="toggleSelection(a)"
										ng-true-value="{{a.associateID}}"></td>
									<td>{{a.associateName}}</td>
									<td>{{a.associateID}}</td>
									<td>{{a.status}}</td>
									<td>{{a.batch.startDate}}</td>
									<td>{{a.batch.endDate}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-6">

						<label id="availableRadio" class="radio-inline"> <input
							ng-model="modifyStatus.status" type="radio" value="Available"
							name="optradio" id="test3"> Available

						</label> <label id="confirmedRadio" class="radio-inline"> <input
							ng-model="modifyStatus.status" type="radio" value="Confirmed"
							name="optradio" id="test2"> Confirmed
						</label>
					</div>
					<div class="col-md-6">

						<button type="submit" class="btn btn-default">Change Status</button>

						<button type="button" class="btn btn-default" data-dismiss="modal"
							ng-click="infoTable.refresh()">Close</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
<!-----------------------
	 Confirmed .Net associates modal
 ------------------------->
<div id="ConfirmedNetAssociateInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">

			<form ng-submit="onSubmitConfirmed()">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center">Confirmed
						Associates</h4>
				</div>
				<div class="modal-body">

					<div>
						<div class="form-group">
							<label>Search</label> <input type="text" ng-model="search"
								placeholder="search">
						</div>
						<table ng-table="vm.tableParams" show-filter="true"
							class="table table-striped">
							<thead>
								<tr>
									<th>Check</th>
									<th>Name</th>
									<th>Employee ID</th>
									<th>Status</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="a in netConfirmedList |filter:search | limitTo:5">
									<td><input ng-model="associateSelected" type="checkbox"
										ng-checked="exist(a)" ng-click="toggleSelection(a)"
										ng-true-value="{{a.associateID}}"></td>
									<td>{{a.associateName}}</td>
									<td>{{a.associateID}}</td>
									<td>{{a.status}}</td>
									<td>{{a.batch.startDate}}</td>
									<td>{{a.batch.endDate}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default">Move to Available</button>

						<button type="button" class="btn btn-default" data-dismiss="modal"
							ng-click="infoTable.refresh()">Close</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
<!-----------------------
	 Available SDet associates modal
 ------------------------->
<div id="AvailableSDETAssociateInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">

			<form ng-submit="onSubmitAvailable()">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center">Available
						Associates</h4>
				</div>
				<div class="modal-body">

					<div>
						<div class="form-group">
							<label>Search</label> <input type="text" ng-model="search"
								placeholder="search">
						</div>
						<table ng-table="vm.tableParams" show-filter="true"
							class="table table-striped">
							<thead>
								<tr>
									<th>Check</th>
									<th>Name</th>
									<th>Employee ID</th>
									<th>Status</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
							</thead>
							<tbody>
								<tr
									ng-repeat="a in sdetAvailableList |filter:search | limitTo:5">
									<td><input ng-model="associateSelected" type="checkbox"
										ng-checked="exist(a)" ng-click="toggleSelection(a)"
										ng-true-value="{{a.associateID}}"></td>
									<td>{{a.associateName}}</td>
									<td>{{a.associateID}}</td>
									<td>{{a.status}}</td>
									<td>{{a.batch.startDate}}</td>
									<td>{{a.batch.endDate}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-12" style="margin-bottom: 10px;">

						<select ng-model="modifyStatus.clientName" class="form-control"
							id="sel1" name="clients" required>
							<option ng-repeat="t in clientList" value="{{t.clientid}}"
								selected>{{t.name}} {{t.location}}</option>
						</select>

					</div>
					<div class="col-md-12">
						<button type="submit" class="btn btn-default">Map Associates</button>

						<button type="button" class="btn btn-default" data-dismiss="modal"
							ng-click="infoTable.refresh()">Close</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>

<!-----------------------
	 Mapped SDet associates modal
 ------------------------->
<div id="MappedSDETAssociateInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">

			<form ng-submit="onSubmitMapped()">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center">Mapped
						Associates</h4>
				</div>
				<div class="modal-body">

					<div>
						<div class="form-group">
							<label>Search</label> <input type="text" ng-model="search"
								placeholder="search">
						</div>
						<table ng-table="vm.tableParams" show-filter="true"
							class="table table-striped">
							<thead>
								<tr>
									<th>Check</th>
									<th>Name</th>
									<th>Employee ID</th>
									<th>Status</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="a in sdetMappedList |filter:search | limitTo:5">
									<td><input ng-model="associateSelected" type="checkbox"
										ng-checked="exist(a)" ng-click="toggleSelection(a)"
										ng-true-value="{{a.associateID}}"></td>
									<td>{{a.associateName}}</td>
									<td>{{a.associateID}}</td>
									<td>{{a.status}}</td>
									<td>{{a.batch.startDate}}</td>
									<td>{{a.batch.endDate}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-6">

						<label id="availableRadio" class="radio-inline"> <input
							ng-model="modifyStatus.status" type="radio" value="Available"
							name="optradio" id="test3"> Available

						</label> <label id="confirmedRadio" class="radio-inline"> <input
							ng-model="modifyStatus.status" type="radio" value="Confirmed"
							name="optradio" id="test2"> Confirmed
						</label>
					</div>
					<div class="col-md-6">

						<button type="submit" class="btn btn-default">Change Status</button>

						<button type="button" class="btn btn-default" data-dismiss="modal"
							ng-click="infoTable.refresh()">Close</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
<!-----------------------
	 Confirmed SDet associates modal
 ------------------------->
<div id="ConfirmedSDETAssociateInfo" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">

			<form ng-submit="onSubmitConfirmed()">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center">Confirmed
						Associates</h4>
				</div>
				<div class="modal-body">

					<div>
						<div class="form-group">
							<label>Search</label> <input type="text" ng-model="search"
								placeholder="search">
						</div>
						<table ng-table="vm.tableParams" show-filter="true"
							class="table table-striped">
							<thead>
								<tr>
									<th>Check</th>
									<th>Name</th>
									<th>Employee ID</th>
									<th>Status</th>
									<th>Start Date</th>
									<th>End Date</th>
								</tr>
							</thead>
							<tbody>
								<tr
									ng-repeat="a in sdetConfirmedList |filter:search | limitTo:5">
									<td><input ng-model="associateSelected" type="checkbox"
										ng-checked="exist(a)" ng-click="toggleSelection(a)"
										ng-true-value="{{a.associateID}}"></td>
									<td>{{a.associateName}}</td>
									<td>{{a.associateID}}</td>
									<td>{{a.status}}</td>
									<td>{{a.batch.startDate}}</td>
									<td>{{a.batch.endDate}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default">Move to Available</button>

						<button type="button" class="btn btn-default" data-dismiss="modal"
							ng-click="infoTable.refresh()">Close</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
</div>