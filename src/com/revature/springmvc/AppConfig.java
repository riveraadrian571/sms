package com.revature.springmvc;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.revature.cache.CacheConfig;
import com.revature.dao.DAOImpl;
import com.revature.dao.DAOService;
import com.revature.model.AssociateInfo;
import com.revature.security.AuthSuccessHandler;
import com.revature.security.SecurityConfig;

@EnableWebMvc //enables the extension of the WebMvcConfigurerAdapter and overriden method: configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
@Configuration //Indicates that a class declares one or more @Bean methods and may be 
//processed by the Spring container to generate bean definitions and service requests for those beans at runtime
@ComponentScan({ "com.revature.controllers"}) //scans for all the controllers
@EnableTransactionManagement //enables the use of beans that allow for db transactions
@Import({ SecurityConfig.class})
/*
 * this class represents the application context of the application or the 
 * central location where the beans or objects are loaded and instantiated 
 * in the Spring ioc container
 */
public class AppConfig extends WebMvcConfigurerAdapter
{
	/*
	 * Configure a handler to delegate unhandled requests by forwarding to the Servlet container's "default" servlet. 
	 * A common use case for this is when the DispatcherServlet is mapped to "/" thus overriding the Servlet container's 
	 * default handling of static resources. 
	 * Following error was resolved providing method below: PageNotFound:1176 - No mapping found for HTTP request with URI 
	 * [/StagingManagementSystem/css/login.css] in DispatcherServlet with name 'dispatcher'
	 * */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) 
	{
		configurer.enable();
	}
	
	@Bean
	public InternalResourceViewResolver viewResolver() 
	{
		InternalResourceViewResolver viewResolver
                          = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}
	
	/*
	 * This method simply allows for the login page to load at the start of the application
	 * */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) 
	{
	    registry.addViewController("/").setViewName("login");
	}
	
	//<bean id="authSuccessHandler" class="com.revature.security.AuthSuccessHandler" />
	@Bean(name="auth")
	public AuthSuccessHandler getAuthSuccessHander()
	{
		return new AuthSuccessHandler();
	}

	//datasource bean gets the data needed in order to have access to the DB
	//additionally, the oracle driver used in this app uses a physical jar file
	//where the build path was configured appropriately
	@Bean(name = "dataSource")
	public DataSource getDataSource() 
	{
	    BasicDataSource dataSource = new BasicDataSource();
	    dataSource.setDriverClassName("oracle.jdbc.OracleDriver");
	    dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:xe");
	    dataSource.setUsername("SYSTEM");
	    dataSource.setPassword("Sophi@rod1oracle");
	    return dataSource;
	}		
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) 
	{
	    LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);		 
	    //sessionBuilder.addAnnotatedClasses(Clowns.class);	    
		//if you want more classes added example below
		//sessionBuilder.addAnnotatedClasses(AssociateInfo.class);
		//if you want to scan packages instead example below
		sessionBuilder.scanPackages("com.revature.model"); //scans packages to find classes with database annotations
		//sessionBuilder.setPackagesToScan(new String[] { "com.revature.classes" });	    
		//to sete a property and show query
		//sessionBuilder.setProperty("hibernate.show_sql", "true");	 
		//to set a group of properties, create a method and call it shown below
		sessionBuilder.addProperties(getHibernateProperties());	        
		return sessionBuilder.buildSessionFactory();	    
	}
			
	//this method is called by the above method to create the sessionfactory in its entirety
	private Properties getHibernateProperties() 
	{
	    Properties properties = new Properties();
	    properties.put("hibernate.show_sql", "true");
	    properties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
	    properties.put("hibernate.hbm2ddl.auto", "update");
	    properties.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
	    return properties;
	}
			
	/*
	 * This transaction manager is appropriate for applications that use a single Hibernate SessionFactory 
	 * for transactional data access, but it also supports direct DataSource access within a transaction (i.e. 
	 * plain JDBC code working with the same DataSource). This allows for mixing services which access Hibernate 
	 * and services which use plain JDBC (without being aware of Hibernate)!
	 */
	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) 
	{
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}
			
	//sessionfactory gets injected onto the daoImpl object
	//@Autowired	//wondering if this is even needed?
	@Bean(name="DAOImpl")
	public DAOService getDAOService(SessionFactory sessionFactory)
	{
		return new DAOImpl(sessionFactory);
	}
}
